FROM sgrio/java:jdk_8_alpine

RUN mkdir -p /springboot-docker-deploy

WORKDIR /springboot-docker-deploy

ARG JAR_FILE=target/springboot-docker-deploy-1.0-SNAPSHOT.jar

COPY ${JAR_FILE} app.jar

EXPOSE 8081

ENV TZ=Asia/Shanghai JAVA_OPTS="-Xms128m -Xmx256m -Djava.security.egd=file:/dev/./urandom"

CMD sleep 30; java -jar app.jar $JAVA_OPTS
