package cn.mawenda.docker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: Ma.wenda
 * @Date: 2023-12-22 14:46
 * @Version: 1.0
 **/
@RestController
@RequestMapping("/hello" )
public class HelloController {

    @GetMapping
    public String hello() {
        return "Hello Docker";
    }
}
