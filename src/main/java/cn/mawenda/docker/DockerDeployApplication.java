package cn.mawenda.docker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: Ma.wenda
 * @Date: 2023-12-22 14:44
 * @Version: 1.0
 **/
@SpringBootApplication
public class DockerDeployApplication {
    public static void main(String[] args) {
        SpringApplication.run(DockerDeployApplication.class, args);
    }
}